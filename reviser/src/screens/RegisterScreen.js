import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'
import { Container, Content, Input, Icon, Form, Item, Thumbnail, } from 'native-base'
import {
    StyleSheet,
    View,
    Text,
    Button,
    ImageBackground,
    TouchableHighlight,
} from 'react-native'

//import images
var background = require('../images/background.jpg')
var logo = require('../images/logo.png')

export default class RegisterScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            password: '',
            c_password: '',
            loading: false,
        }
    }

    async callAPI() {
        try {
            var formData = new FormData()
            formData.append('name', this.state.name)
            formData.append('email', this.state.email)
            formData.append('password', this.state.password)
            formData.append('c_password', this.state.c_password)
            let response = await fetch('http://52.59.255.90/api/register', {
                method: 'POST',
                body: formData
            })

            let data = await response.json()
            return data
        } catch (error) {
            console.log(error)
        }
    }

    Register() {
        this.setState({ loading: !this.state.loading })
        this.callAPI()
            .then((data) => {
                this.setState({ loading: !this.state.loading })
                if (data['status'] == 'success') {
                    this.props.navigation.navigate('Recorder', {
                        token: data['token']
                    })
                }
                else {
                    alert('some fields are missing or wrong')
                }
            })
    }

    render() {
        return (
            <ImageBackground source={background} style={styles.background}>
                <Thumbnail large source={logo} style={styles.thumbnail} />
                <Form>
                    <Item style={styles.item}>
                        <Icon name='person' style={styles.icon} />
                        <Input
                            onChangeText={(name) => {
                                this.setState({ name })
                            }}
                            placeholder='Name'
                            placeholderTextColor='white'
                            style={styles.input}
                        />
                    </Item>
                    <Item style={styles.item}>
                        <Icon name='mail' style={styles.icon} />
                        <Input
                            onChangeText={(email) => {
                                this.setState({ email })
                            }}
                            placeholder='Email'
                            placeholderTextColor='white'
                            style={styles.input}
                        />
                    </Item>
                    <Item style={styles.item}>
                        <Icon name='lock' style={styles.icon} />
                        <Input
                            onChangeText={(password) => {
                                this.setState({ password })
                            }}
                            placeholder='Password'
                            placeholderTextColor='white'
                            secureTextEntry={true}
                            style={styles.input}
                        />
                    </Item>
                    <Item style={styles.item}>
                        <Icon name='lock' style={styles.icon} />
                        <Input
                            onChangeText={(c_password) => {
                                this.setState({ c_password })
                            }}
                            placeholder='Confirm Password'
                            placeholderTextColor='white'
                            secureTextEntry={true}
                            style={styles.input}
                        />
                    </Item>
                    <Button
                        title='Sign Up'
                        onPress={() => {
                            this.Register()
                        }}
                        style={styles.button}
                    />
                </Form>
            </ImageBackground>
        )
    }

}

const styles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        alignItems: 'center',
        justifyContent: 'center',
    },
    thumbnail: {
        marginBottom: 20,
    },
    item: {
        marginBottom: 20,
        width: '75%',
    },
    icon: {
        color: 'white',
    },
    input: {
        fontSize: 14,
        color: 'white',
        fontFamily: 'Ubuntu'
    },
    button: {
        alignSelf: 'center',
    },
    touch: {
        marginTop: 20,
        alignSelf: 'center',
        flexDirection: 'row',
    },
    text: {
        fontSize: 12,
    },
    underline: {
        fontSize: 12,
        textDecorationLine: 'underline',
    }
})
