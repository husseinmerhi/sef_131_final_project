import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'
import { StyleSheet, View, Text, Image, AsyncStorage, } from 'react-native'
import { Container, Content, Card, CardItem, Body, Icon, SwipeRow, Button } from 'native-base'


//import components
import Row from '../components/Row'

export default class LibraryScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            files: [],
            status: '',
            loading: false,
        }
    }

    async componentDidMount() {
        this.Library()
    }

    async callLibraryAPI() {
        try {
            let response = await fetch('http://52.59.255.90/api/record/list', {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + this.props.navigation.getParam('token'),
                }
            })

            let data = await response.json()
            return data
        } catch (error) {
            console.log(error)
        }
    }

    async callRemoveAPI(id) {
        try {
            let response = await fetch('http://52.59.255.90/api/record/remove', {
                method: 'DELETE',
                headers: {
                    'Authorization': 'Bearer ' + this.props.navigation.getParam('token'),
                    'id': id
                }
            })

            let data = await response.json()
            return data
        } catch (error) {
            console.log(error)
        }
    }

    Library() {
        this.setState({ loading: !this.state.loading })
        this.callLibraryAPI()
            .then((data) => {
                this.setState({ loading: !this.state.loading })
                if (data['status'] == 'success') {
                    this.setState({
                        files: data['records']
                    })
                }
                else {
                    this.setState({ status: data['status'] })
                }
            })
    }

    removeRecord(id) {
        this.setState({ loading: !this.state.loading })
        this.callRemoveAPI(id)
            .then((data) => {
                this.setState({ loading: !this.state.loading })
                if (data['status'] == 'success') {
                    console.log(data['status'])
                }
            })
    }

    deleteAudio(key) {
        this.removeRecord(this.state.files[key].id)
        this.state.files.splice(key, 1)
        this.setState({ files: this.state.files })
    }

    render() {

        const rows = this.state.files.map((val, key) => {
            return <Row
                key={key}
                keyval={key}
                fileName={val.record_name}
                filePath={val.record_path}
                date={val.created_at}
                length={val.length}
                navigation={this.props.navigation}
                deleteMethod={() => this.deleteAudio(key)}
            />
        })

        return (
            <Container style={styles.background} >
                <Content padder>
                    <Text style={styles.title}>My Records</Text>
                    <Text style={styles.status}>{this.state.status}</Text>
                    <Text style={styles.title}>{this.state.token}</Text>
                    {rows}
                </Content>
            </Container>
        )

    }

}

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#2196F3',
    },
    title: {
        fontFamily: 'Ubuntu',
        color: 'white',
        alignSelf: 'center',
        marginVertical: 5,
    },
    status: {
        fontFamily: 'Ubuntu',
        color: 'red',
        alignSelf: 'center',
        marginVertical: 5,
    },
})
