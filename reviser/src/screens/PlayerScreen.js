import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'
import { Button, Icon, Input, Form, Item } from 'native-base'
import { StyleSheet, View, Text, AsyncStorage, ToastAndroid } from 'react-native'
import Sound from 'react-native-sound'

export default class PlayerScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            secondsElapsed: 0,
            length: 0,
        }
        this.timer = null
        this.recordPath = this.props.navigation.getParam('recordPath')
        this.recordName = this.props.navigation.getParam('recordName')
    }

    play() {
        let onlinePath = 'http://52.59.255.90/storage/' + this.recordPath
        var player = new Sound(onlinePath, '', error => {
            if (error) {
                console.log('failed to load the sound', error)
                return
            }
            this.startTime()
            Sound.setCategory('Playback')
            player.play(success => {
                if (success) {
                    console.log('successfully finished playing')
                } else {
                    console.log('playback failed due to audio decoding errors')
                }
                this.stopTime()
                Sound.setCategory('Record')
                player.release()
            })
        })
    }

    startTime() {
        this.timer = setInterval(() => {
            this.setState({ secondsElapsed: this.state.secondsElapsed + 1 })
        }, 1000)
    }

    stopTime() {
        this.setState({ length: this.state.secondsElapsed })
        clearInterval(this.timer)
        this.setState({ secondsElapsed: 0 })
    }

    generateSeconds(seconds) {
        let secs = seconds % 60;
        if (secs < 10) {
            secs = '0' + secs
        }
        return secs
    }

    generateMinutes(seconds) {
        let mins = parseInt(seconds / 60);
        if (mins < 10) {
            mins = '0' + mins
        }
        return mins
    }

    render() {
        return (
            <View style={styles.background}>
                <View style={styles.console}>
                    <Text style={styles.title}>{this.recordName}</Text>
                    <View style={styles.timer}>
                        <Text style={styles.digit}>0</Text>
                        <Text style={styles.unit}>H</Text>
                        <Text style={styles.digit}>{this.generateMinutes(this.state.secondsElapsed)}</Text>
                        <Text style={styles.unit}>M</Text>
                        <Text style={styles.digit}>{this.generateSeconds(this.state.secondsElapsed)}</Text>
                        <Text style={styles.unit}>S</Text>
                    </View>
                    <View style={styles.timer}>
                        <Text style={styles.digit}>0</Text>
                        <Text style={styles.unit}>H</Text>
                        <Text style={styles.digit}>00</Text>
                        <Text style={styles.unit}>M</Text>
                        <Text style={styles.digit}>0{this.props.navigation.getParam('length')}</Text>
                        <Text style={styles.unit}>S</Text>
                    </View>
                    <View>
                    </View>
                </View>
                <View style={styles.control}>
                    <Button onPress={() => { this.play() }} style={styles.buttonPlay} rounded>
                        <Icon type='Entypo' name='controller-play' style={styles.iconPlay} />
                    </Button>
                    <Button onPress={() => { }} style={styles.buttonSecondry} rounded>
                        <Icon type='Entypo' name='controller-paus' style={styles.iconSecondry} />
                    </Button>
                    <Button onPress={() => { this.props.navigation.navigate('Library') }} style={styles.buttonSecondry} rounded>
                        <Icon type='Entypo' name='folder-music' style={styles.iconSecondry} />
                    </Button>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#2196F3'
    },
    console: {
        width: '100%',
        height: '70%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    control: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        fontFamily: 'Ubuntu',
        color: 'white',
        marginBottom: 20,
    },
    item: {
        width: '75%',
        marginBottom: 50,
    },
    icon: {
        color: 'white',
    },
    input: {
        fontFamily: 'Ubuntu',
        fontSize: 15,
        color: 'white'
    },
    timer: {
        flexDirection: 'row',
        marginBottom: 40,
    },
    digit: {
        fontFamily: 'Ubuntu',
        fontSize: 25,
        color: 'white',
    },
    unit: {
        fontFamily: 'Ubuntu',
        color: 'white',
    },
    buttonPlay: {
        width: 80,
        height: 80,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    buttonSecondry: {
        width: 50,
        height: 50,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        marginLeft: 40,
    },
    iconPlay: {
        fontSize: 35,
        alignSelf: 'center',
        color: 'red',
    },
    iconSecondry: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'black',
    },
})
