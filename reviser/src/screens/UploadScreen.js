import React, { Component } from 'react'
import { View, Text, Button, AsyncStorage } from 'react-native'

export default class UploadScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            path: '',
        }
    }
    async componentDidMount() {
        let myJSON = await AsyncStorage.getItem('greeting')
        data = JSON.parse(myJSON)
        this.setState({ path: data.recordPath })
    }
    upload = async () => {
        console.log('uploading')
        const filepath = 'file://' + this.state.path
        const formData = new FormData()
        formData.append('file', {
            uri: filepath,
            name: 'greeting.wav',
            type: 'audio/wav',
        })
        try {
            const res = await fetch('http://192.168.88.225/api/file/store', {
                method: 'POST',
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
                body: formData,
            })
            const json = await res.json()
            console.log(json)
        } catch (err) {
            console.log(err)
        }
    }
    render() {
        return (
            <View>
                <Text>Upload File</Text>
                <Text>{this.state.path}</Text>
                <Button title='Upload' onPress={() => { this.upload() }} />
            </View>
        )
    }
}