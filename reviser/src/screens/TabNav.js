//import screens
import RecorderScreen from './RecorderScreen'
import LibraryScreen from './LibraryScreen'
import PlayerScreen from './PlayerScreen'

//import utilities
import React, { Component } from 'react'
import { createBottomTabNavigator } from 'react-navigation'

export default class TabNav extends Component {
    
    constructor(props) {
        super(props)
        token = this.props.navigation.getParam('token')   
    }
    render() {
        return (
            <TabNavigator />
        )
    }
}

const TabNavigator = createBottomTabNavigator({
    Library: {
        screen: props => <LibraryScreen token={this.token}/>,
    },
    Recorder: {
        screen: props => <RecorderScreen token={this.token}/>,
    },
    Player: {
        screen: PlayerScreen,
    },
})
