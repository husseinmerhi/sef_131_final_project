import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'
import {
    Container,
    Content,
    Input,
    Icon,
    Form,
    Item,
    Thumbnail,
} from 'native-base'
import {
    StyleSheet,
    View,
    Text,
    Button,
    ImageBackground,
    TouchableHighlight,
    StatusBar,
    ActivityIndicator,
} from 'react-native'

//import images
var background = require('../images/background.jpg')
var logo = require('../images/logo.png')

export default class LoginScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            email: '',
            password: '',
        }
    }

    async callAPI() {
        try {
            let response = await fetch('http://52.59.255.90/api/login', {
                method: 'GET',
                headers: {
                    'email': this.state.email,
                    'password': this.state.password
                }
            })

            let data = await response.json()
            return data
        } catch (error) {
            console.log(error)
        }
    }

    Auth() {
        this.setState({ loading: !this.state.loading })
        this.callAPI()
            .then((data) => {
                this.setState({ loading: !this.state.loading })
                if (data['status'] == 'success') {
                    this.props.navigation.navigate('Recorder', {
                        token: data['token']
                    })
                }
                else {
                    alert(data['message'])
                }
            })
    }

    render() {

        let buttonStatus = this.state.loading
            ? <ActivityIndicator large style={styles.button} />
            : <Button
                title='Sign In'
                onPress={() => {
                    this.Auth()
                }}
                style={styles.button}
            />

        return (
            <ImageBackground source={background} style={styles.background}>
                <StatusBar hidden={true} />
                <Thumbnail large source={logo} style={styles.thumbnail} />
                <Form>
                    <Item style={styles.item}>
                        <Icon name='mail' style={styles.icon} />
                        <Input
                            placeholder='Email'
                            placeholderTextColor='white'
                            onChangeText={(email) => {
                                this.setState({ email })
                            }}
                            style={styles.input}
                        />
                    </Item>
                    <Item style={styles.item}>
                        <Icon name='lock' style={styles.icon} />
                        <Input
                            placeholder='Password'
                            placeholderTextColor='white'
                            secureTextEntry={true}
                            onChangeText={(password) => {
                                this.setState({ password })
                            }}
                            style={styles.input}
                        />
                    </Item>
                    {buttonStatus}
                    <View style={styles.touch}>
                        <Text style={styles.text}>Dont you have an account?</Text>
                        <TouchableHighlight
                            onPress={() => {
                                this.props.navigation.navigate('Register')
                            }}
                        >
                            <Text style={styles.underline}> Sign Up </Text>
                        </TouchableHighlight>
                    </View>
                </Form>
            </ImageBackground>
        )
    }

}

const styles = StyleSheet.create({
    background: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    thumbnail: {
        marginBottom: 20,
    },
    item: {
        marginBottom: 20,
        width: '75%',
    },
    icon: {
        color: 'white',
    },
    button: {
        alignSelf: 'center',
        fontFamily: 'Ubuntu',
        color: 'white'
    },
    input: {
        fontSize: 14,
        color: 'white',
        fontFamily: 'Ubuntu'
    },
    touch: {
        marginTop: 20,
        alignSelf: 'center',
        flexDirection: 'row',
    },
    text: {
        fontSize: 12,
        fontFamily: 'Ubuntu',
    },
    underline: {
        fontSize: 12,
        textDecorationLine: 'underline',
        fontFamily: 'Ubuntu',
    },
})
