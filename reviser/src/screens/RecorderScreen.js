import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'
import { Button, Icon, Input, Form, Item } from 'native-base'
import { StyleSheet, View, Text, AsyncStorage, ToastAndroid } from 'react-native'
import AudioRecord from 'react-native-audio-record'
import Sound from 'react-native-sound'

export default class RecorderScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            fileName: '',
            recordName: '',
            recordPath: '',
            recording: false,
            playing: false,
            secondsElapsed: 0,
            length: 0,
        }
        this.timer = null
    }

    recordingProcess() {
        if (this.state.recording) {
            this.stop()
        }
        else {
            this.record()
        }
    }

    record() {
        var options = {
            sampleRate: 16000,
            channels: 1,
            bitsPerSample: 16,
            wavFile: this.state.fileName + '.wav'
        }
        AudioRecord.init(options)
        this.setState({ recordName: this.state.fileName, recording: true })
        this.startTime()
        AudioRecord.start()
    }

    async stop() {
        this.stopTime()
        let recordPath = await AudioRecord.stop()
        this.setState({ recordPath, recording: false, fileName: '' })
        this.save()
        this.upload(this.state.recordPath, this.state.recordName)
        this.refs.Input.setNativeProps({ text: '' })
    }

    async upload(recordPath, recordName) {
        const filepath = 'file://' + recordPath
        const formData = new FormData()
        formData.append('file', {
            uri: filepath,
            name: recordName + '.wav',
            type: 'audio/wav',
        })
        formData.append('name', recordName)
        formData.append('length', this.state.length)
        try {
            const res = await fetch('http://52.59.255.90/api/record/upload', {
                method: 'POST',
                headers: {
                    'Authorization': 'Bearer ' + this.props.navigation.getParam('token'),
                    'Content-Type': 'multipart/form-data',
                },
                body: formData,
            })
            const json = await res.json()
        } catch (err) {
            console.log(err)
        }
    }

    play() {
        this.setState({ playing: true })
        this.startTime()
        var player = new Sound(this.state.recordPath, '', error => {
            if (error) {
                console.log('failed to load the sound', error)
                return
            }
            Sound.setCategory('Playback')
            player.play(success => {
                if (success) {
                    this.stopTime()
                    this.setState({ playing: false })
                    console.log('successfully finished playing')
                } else {
                    console.log('playback failed due to audio decoding errors')
                }
                Sound.setCategory('Record')
                player.release()
            })
        })
    }

    startTime() {
        this.timer = setInterval(() => {
            this.setState({ secondsElapsed: this.state.secondsElapsed + 1 })
        }, 1000)
    }

    stopTime() {
        this.setState({ length: this.state.secondsElapsed })
        clearInterval(this.timer)
        this.setState({ secondsElapsed: 0 })
    }

    async save() {
        let meta = {
            recordPath: this.state.recordPath,
            date: new Date().toDateString(),
            length: this.state.length
        }
        this.setNote(this.state.recordName, JSON.stringify(meta))
        this.setState({ decision: false })
        ToastAndroid.show('Saved', ToastAndroid.SHORT)
    }

    async setNote(key, value) {
        try {
            await AsyncStorage.setItem(key, value)
        } catch (error) {
            console.log("Error saving data" + error)
        }
    }

    generateSeconds(seconds) {
        let secs = seconds % 60;
        if (secs < 10) {
            secs = '0' + secs
        }
        return secs
    }

    generateMinutes(seconds) {
        let mins = parseInt(seconds / 60);
        if (mins < 10) {
            mins = '0' + mins
        }
        return mins
    }

    render() {
        let recordButton = this.state.recording
            ? <Icon type='Entypo' name='controller-stop' style={styles.iconRecord} />
            : <Icon type='Entypo' name='mic' style={styles.iconRecord} />
        let playButton = this.state.playing
            ? <Icon type='Entypo' name='controller-paus' style={styles.iconSecondry} />
            : <Icon type='Entypo' name='controller-play' style={styles.iconSecondry} />
        let textStatus = this.state.recording
            ? 'recording ' + this.state.fileName
            : 'tap to start recording'
        return (
            <View style={styles.background}>
                <View style={styles.console}>
                    <Form>
                        <Item style={styles.item}>
                            <Icon type='Entypo' name='music' style={styles.icon} />
                            <Input
                                ref='Input'
                                placeholder='filename'
                                placeholderTextColor='white'
                                onChangeText={(fileName) => { this.setState({ fileName }) }}
                                style={styles.input}
                            />
                        </Item>
                    </Form>
                    <View style={styles.timer}>
                        <Text style={styles.digit}>0</Text>
                        <Text style={styles.unit}>H</Text>
                        <Text style={styles.digit}>{this.generateMinutes(this.state.secondsElapsed)}</Text>
                        <Text style={styles.unit}>M</Text>
                        <Text style={styles.digit}>{this.generateSeconds(this.state.secondsElapsed)}</Text>
                        <Text style={styles.unit}>S</Text>
                    </View>
                    <View>
                        <Text style={styles.unit}>{textStatus}</Text>
                    </View>
                </View>
                <View style={styles.control}>
                    <Button onPress={() => { this.recordingProcess() }} style={styles.buttonRecord} rounded>
                        {recordButton}
                    </Button>
                    <Button onPress={() => { this.play() }} style={styles.buttonSecondry} rounded>
                        {playButton}
                    </Button>
                    <Button
                        onPress={() => {
                            this.props.navigation.navigate('Library', {
                                token: this.props.navigation.getParam('token')
                            })
                        }}
                        style={styles.buttonSecondry}
                        rounded
                    >
                        <Icon type='Entypo' name='folder-music' style={styles.iconSecondry} />
                    </Button>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    background: {
        backgroundColor: '#2196F3'
    },
    console: {
        width: '100%',
        height: '70%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    control: {
        width: '100%',
        height: '30%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    item: {
        width: '75%',
        marginBottom: 50,
    },
    icon: {
        color: 'white',
    },
    input: {
        fontFamily: 'Ubuntu',
        fontSize: 15,
        color: 'white'
    },
    timer: {
        flexDirection: 'row',
        marginBottom: 20,
    },
    digit: {
        fontFamily: 'Ubuntu',
        fontSize: 25,
        color: 'white',
    },
    unit: {
        fontFamily: 'Ubuntu',
        color: 'white',
    },
    buttonRecord: {
        width: 80,
        height: 80,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    buttonSecondry: {
        width: 50,
        height: 50,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        marginLeft: 40,
    },
    iconRecord: {
        fontSize: 35,
        alignSelf: 'center',
        color: 'red',
    },
    iconSecondry: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'black',
    },
})
