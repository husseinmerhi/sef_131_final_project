//import screens
import LoginScreen from './screens/LoginScreen'
import RegisterScreen from './screens/RegisterScreen'
import LibraryScreen from './screens/LibraryScreen'
import RecorderScreen from './screens/RecorderScreen'
import PlayerScreen from './screens/PlayerScreen'


//import utilities
import React, { Component } from 'react'
import { createStackNavigator } from 'react-navigation'

export default class App extends Component {
    render() {
        return (
            <StackNavigator />
        )
    }
}

const StackNavigator = createStackNavigator({
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            header: null,
        },
    },
    Register: {
        screen: RegisterScreen,
        navigationOptions: {
            header: null,
        },
    },
    Library: {
        screen: LibraryScreen,
        navigationOptions: {
            header: null,
        },
    },
    Recorder: {
        screen: RecorderScreen,
        navigationOptions: {
            header: null,
        },
    },
    Player: {
        screen: PlayerScreen,
        navigationOptions: {
            header: null,
        },
    },
})
