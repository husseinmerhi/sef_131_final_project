import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Button } from 'native-base'
import Sound from 'react-native-sound'

export default class Record extends Component {

    generateLength() {
        let length = ''
        secs = this.props.length % 60
        mins = parseInt(this.props.length / 60)
        if (mins < 10) {
            length += '0'
        }
        length += mins + ':'
        if (secs < 10) {
            length += '0'
        }
        length += secs
        return length
    }

    render() {
        return (
            <Button
                transparent
                onPress={() => {
                    this.props.navigation.navigate('Player', {
                        recordPath: this.props.filePath,
                        recordName: this.props.fileName,
                        length: this.props.length
                    })
                }}
                style={[styles.button, styles.column]}
            >
                <View>
                    <Text style={styles.name}>{this.props.fileName}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.meta}>{this.props.date}</Text>
                    <Text style={styles.meta}>  -  </Text>
                    <Text style={styles.meta}>{this.generateLength()}</Text>
                </View>
            </Button>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'white',
        width: '90%',
    },
    column: {
        alignItems: 'flex-start',
        flexDirection: 'column',
    },
    row: {
        flexDirection: 'row',
    },
    name: {
        fontFamily: 'Ubuntu',
        fontSize: 15,
        color: 'black',
    },
    meta: {
        fontFamily: 'Ubuntu',
        fontSize: 10,
    }
})