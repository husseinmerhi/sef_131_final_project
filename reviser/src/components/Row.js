import React, { Component } from 'react'
import { StackNavigator } from 'react-navigation'
import { StyleSheet } from 'react-native'
import { Card, CardItem, Body, Icon, SwipeRow, Button } from 'native-base'

//import components
import Record from '../components/Record'

export default class Row extends Component {

    render() {
        return (
            <Card>
                <CardItem style={styles.cardItem}>
                    <Body style={styles.body} >
                        <Icon type='Entypo' name='music' style={styles.icon} />
                        <SwipeRow
                            style={styles.swipe}
                            body={
                                <Record
                                    fileName={this.props.fileName}
                                    filePath={this.props.filePath}
                                    date={this.props.date}
                                    length={this.props.length}
                                    navigation={this.props.navigation}
                                />
                            }
                            rightOpenValue={-50}
                            right={
                                <Button
                                    transparent
                                    onPress={this.props.deleteMethod}
                                    style={styles.trashButton}
                                >
                                    <Icon type='Entypo' name='trash' style={styles.trash} />
                                </Button>
                            }
                        />
                    </Body>
                </CardItem>
            </Card>
        )
    }

}

const styles = StyleSheet.create({
    cardItem: {
        padding: 0,
        margin: 0,
        height: 50,
    },
    body: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    icon: {
        color: '#2196F3',
        marginRight: 10,
    },
    swipe: {
        width: '95%',
    },
    trashButton: {
        padding: 0,
        margin: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    trash: {
        fontSize: 20,
        color: 'red',
    },
})
