import { AppRegistry } from 'react-native';
import App from './src/App';

import { YellowBox } from 'react-native'
console.disableYellowBox = true

AppRegistry.registerComponent('reviser', () => App);
