<?php

namespace cloud;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function records() {
        return $this->hasMany('cloud\Record');
    }

    public function friends() {
        return $this->hasMany('cloud\Friend');
    }

    public function shares() {
        return $this->hasMany('cloud\Share');
    }
}