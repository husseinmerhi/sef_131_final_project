<?php

namespace cloud\Http\Controllers;

use cloud\User;
use cloud\Record;
use cloud\Friend;
use cloud\Share;

class CloudController extends Controller
{
    public function index()
    {
        $users = User::all();
        $records = Record::all();
        $friends = Friend::all();
        $shares = Share::all();
        return view('cloud')->with([
            'users' => $users,
            'records' => $records,
            'friends' => $friends,
            'shares' => $shares
        ]);
    }
}
