<?php

namespace cloud\Http\Controllers\API;

use Illuminate\Http\Request;
use cloud\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use cloud\Friend;

class FriendController extends Controller
{

    public function list(Request $request) {

        $user = Auth::user();
        $friends = $user->friends;
        if (count($friends) > 0) {
            return response()
                ->json(['status' => 'success', 'friends' => $friends], 200)
                ->header('Access-Control-Allow-Origin' ,'*');
        }
        else {
            return response()
                ->json(['status' => 'empty'], 200)
                ->header('Access-Control-Allow-Origin' ,'*');
        }

    }

    public function add(Request $request) {

        $friend = new Friend();
        $friend->user_id = Auth::user()->id;
        $friend->friend_id = $request->input('id');
        $friend->save();
        return response()
            ->json(['status' => 'success'], 200)
            ->header('Access-Control-Allow-Origin', '*');

    }

    public function remove(Request $request) {

        $friend = Friend::find($request->header('id'));
        $friend->delete();
        return response()
            ->json(['status' => 'success'], 200)
            ->header('Access-Control-Allow-Origin', '*');

    }

}
