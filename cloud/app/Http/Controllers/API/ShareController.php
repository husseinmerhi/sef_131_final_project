<?php

namespace cloud\Http\Controllers\API;

use Illuminate\Http\Request;
use cloud\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use cloud\Share;

class ShareController extends Controller
{

    public function list() {

        $user = Auth::user();
        $shares = $user->shares;
        if (count($shares) > 0) {
            return response()
                ->json(['status' => 'success', 'shares' => $shares], 200)
                ->header('Access-Control-Allow-Origin' ,'*');
        }
        else {
            return response()
                ->json(['status' => 'empty'], 200)
                ->header('Access-Control-Allow-Origin' ,'*');
        }

    }

    public function add(Request $request) {

        $share = new Share();
        $share->user_id = $request->input('friend_id');
        $share->friend_id = Auth::user()->id;
        $share->record_id = $request->input('record_id');
        $share->save();
        return response()
            ->json(['status' => 'success'], 200)
            ->header('Access-Control-Allow-Origin', '*');

    }

    public function remove(Request $request) {

        $share = Share::find($request->header('id'));
        $share->delete();
        return response()
            ->json(['status' => 'success'], 200)
            ->header('Access-Control-Allow-Origin', '*');

    }

}
