<?php

namespace cloud\Http\Controllers\API;

use Illuminate\Http\Request;
use cloud\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
use cloud\User;
use cloud\Friend;

class UserController extends Controller
{

    public function login(Request $request) {
        
        if (Auth::attempt([
            'email' => $request->header('email'),
            'password' => $request->header('password')
        ])) {
            $user = Auth::user();
            $success['status'] = 'success';
            $success['token'] = $user->createToken('melody')->accessToken;
            return response()
                ->json($success, 200)
                ->header('Access-Control-Allow-Origin', '*');
        }
        else {
            $failed['status'] = 'failed';
            $failed['message'] = 'unauthorized';
            return response()
                ->json($failed, 401)
                ->header('Access-Control-Allow-Origin', '*');
        }

    }

    public function register(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            $failed['status'] = 'failed';
            $failed['message'] = $validator->errors();
            return response()->json($failed, 400);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['status'] = 'success';
        $success['token'] = $user->createToken('melody')->accessToken;
        return response()
            ->json($success, 200)
            ->header('Access-Control-Allow-Origin', '*');
    }

    public function profile() {

        return response()
        ->json(['success' => Auth::user()], 200)
        ->header('Access-Control-Allow-Origin', '*');

    }

}
