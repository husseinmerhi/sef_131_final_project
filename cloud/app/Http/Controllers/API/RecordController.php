<?php

namespace cloud\Http\Controllers\API;

use Illuminate\Http\Request;
use cloud\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Validator;
use cloud\Record;

class RecordController extends Controller
{

    public function list(Request $request) {

        $user = Auth::user();
        $records = $user->records;
        if (count($records) > 0) {
            return response()
                ->json(['status' => 'success', 'records' => $records], 200)
                ->header('Access-Control-Allow-Origin', '*');
        }
        else {
            return response()
                ->json(['status' => 'empty'], 200)
                ->header('Access-Control-Allow-Origin', '*');
        }

    }

    public function upload(Request $request) {

        $file = $request->file('file');
        $fileName = $file->getClientOriginalName();
        $record = new Record();
        $record->user_id = Auth::user()->id;
        $record->record_name = $request->input('name');
        $record->record_path = $fileName;
        $record->length = $request->input('length');
        $record->save();
        Storage::disk('public')->put($fileName, File::get($file));
        return response()
            ->json(['status' => 'success'], 200)
            ->header('Access-Control-Allow-Origin', '*');

    }

    public function remove(Request $request) {

        $record = Record::find($request->header('id'));
        $record->delete();
        return response()
            ->json(['status' => 'success'], 200)
            ->header('Access-Control-Allow-Origin', '*');

    }

}
