<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
    //user api calls
    Route::get('user/profile', 'API\UserController@profile');
    //record api calls
    Route::get('record/list' ,'API\RecordController@list');
    Route::post('record/upload', 'API\RecordController@upload');
    Route::delete('record/remove', 'API\RecordController@remove');
    //friend api calls
    Route::get('friend/list', 'API\FriendController@list');
    Route::post('friend/add', 'API\FriendController@add');
    Route::delete('friend/remove', 'API\FriendController@remove');
    //share api calls
    Route::get('share/list', 'API\ShareController@list');
    Route::post('share/add', 'API\ShareController@add');
    Route::delete('share/remove', 'API\ShareController@remove');
});
