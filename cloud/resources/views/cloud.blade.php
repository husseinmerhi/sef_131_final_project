@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <!-- login -->
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
        <!-- users -->
        <div class="col-md-8 mt-4">
            <div class="card">
                <div class="card-header">Users Table</div>
                <div class="card-body">
                    @if (count($users)>0)
                        @foreach ($users as $user)
                            <p>{{$user->id}} - {{$user->name}} - {{$user->email}} - {{$user->password}}</p>
                        @endforeach
                    @else
                        <p>No Users</p>
                    @endif
                </div>
            </div>
        </div>
        <!-- records -->
        <div class="col-md-8 mt-4">
            <div class="card">
                <div class="card-header">Records Table</div>
                <div class="card-body">
                    @if(count($records)>0)
                        @foreach($records as $record)
                            <p>{{$record->id}} - {{$record->user_id}} - {{$record->record_path}} - {{$record->length}}</p>
                        @endforeach
                    @else
                        <p>No Records</p>
                    @endif
                </div>
            </div>
        </div>
        <!-- friends -->
        <div class="col-md-8 mt-4">
            <div class="card">
                <div class="card-header">Friends Table</div>
                <div class="card-body">
                    @if(count($friends)>0)
                        @foreach($friends as $friend)
                            <p>{{$friend->user_id}} - {{$friend->friend_id}}</p>
                        @endforeach
                    @else
                        <p>No Friends</p>
                    @endif
                </div>
            </div>
        </div>
        <!-- shares -->
        <div class="col-md-8 mt-4">
            <div class="card">
                <div class="card-header">Shares Table</div>
                <div class="card-body">
                    @if(count($shares)>0)
                        @foreach($shares as $share)
                            <p>{{$share->id}} - {{$share->user_id}} - {{$share->friend_id}} - {{$share->record_id}}</p>
                        @endforeach
                    @else
                        <p>No Shares</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
